define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('NotificacaoCrudCtrl', NotificacaoCrudCtrl);

  NotificacaoCrudCtrl.$inject = ['NotificacaoResource', '$state', 'GenericCrudService' ];

  function NotificacaoCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;



    vm.editarAtividade = editarAtividadeFn;

    var path = 'notificacao';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Notificação';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Notificação';

    //Título do formulário
    vm.title = 'Detalhes da Atividades';

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idNotificacao', onInit );

    function onInit() {

    }

    function editarAtividadeFn(registroAtividade){
      Resource.update(registroAtividade);
    }

  }
});
