define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('NotificacaoResource', NotificacaoResource);

  //---

  NotificacaoResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function NotificacaoResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/notificacao/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/notificacao/:subpath',
            params : {subpath : 'findwhen'}
        },
        'findPagination':{
          'method' : 'GET' , isArray: true,
          url : rootUrl + '/notificacao/notifica/:id'
        }
      }
    );

    return rest;

  }

});
