define(function() {
  'use strict';

  return {

    'idEmpresa':undefined,
    'dsRazaoSocial':undefined,
    'dsNomeFantasia':undefined,
    'dsResponsavelLegal':undefined,
    'dsCpf':undefined,
    'dsCnpj':undefined,
    'dsNire':undefined,
    'fgStatus':undefined,
    'endereco':undefined,
    'listaAtividadeEmpresa':[

    ],
    'listaFilial':[

    ],
    'listaSocio':[

    ],
    'listaRegularidadeFiscal':[

    ],
    'listaParecer':[

    ],
    'listaContato':[

    ],
    'listaControleInterno':[

    ]
  };

});
