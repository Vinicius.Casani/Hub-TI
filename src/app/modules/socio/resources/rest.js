define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('SocioResource', SocioResource);

  //---

  SocioResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function SocioResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/socio/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/socio/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPagination':{
          method: 'GET', isArray:true,
          url:rootUrl + '/empresa'
        },

      }
    );

    return rest;

  }

});
