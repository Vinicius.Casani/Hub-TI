define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/socio', '/socio/list'); // default

    $stateProvider
      .state('socio', {
        abstract: true,
        url: '/socio',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('socio.list', {
        url: '/list',
        views: {
          'content@socio': {
            templateUrl   : 'app/modules/socio/templates/list.html',
            controller    : 'SocioSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('socio.new', {
        url: '/new',
        views: {
          'content@socio': {
            templateUrl   : 'app/modules/socio/templates/form.html',
            controller    : 'SocioCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('socio.edit', {
        url: '/edit/:id',
        views: {
          'content@socio': {
            templateUrl   : 'app/modules/socio/templates/form.html',
            controller    : 'SocioCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
