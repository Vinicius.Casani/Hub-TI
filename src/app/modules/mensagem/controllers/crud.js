define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('MensagemCrudCtrl', MensagemCrudCtrl);

  MensagemCrudCtrl.$inject = ['MensagemResource', '$state', 'GenericCrudService', '$mdDialog', '$location'];

  /**
  * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
  * @date 02/09/2015
  * @version 1.0
  * */
  function MensagemCrudCtrl(Resource, $state, GenericCrudService, $mdDialog, $location ) {

    var vm = this;

    var path = 'mensagem';

    vm.editarMsg = editarMsgFn;

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Mensagem';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Mensagem';

    //Título do formulário
    vm.title = 'Mensagem';

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idMsgEmail', onInit );

    function onInit() {

    }

    function editarMsgFn(item) {
      delete item.edit;
      Resource.update(item, function() {
      });
    }

  }
});
