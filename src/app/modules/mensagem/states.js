define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/mensagem', '/mensagem/list'); // default

    $stateProvider
      .state('mensagem', {
        abstract: true,
        url: '/mensagem',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('mensagem.list', {
        url: '/list',
        views: {
          'content@mensagem': {
            templateUrl   : 'app/modules/mensagem/templates/list.html',
            controller    : 'MensagemSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('mensagem.new', {
        url: '/new',
        views: {
          'content@mensagem': {
            templateUrl   : 'app/modules/mensagem/templates/form.html',
            controller    : 'MensagemCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('mensagem.edit', {
        url: '/edit/:id',
        views: {
          'content@mensagem': {
            templateUrl   : 'app/modules/mensagem/templates/form.html',
            controller    : 'MensagemCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
