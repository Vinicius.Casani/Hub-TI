define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('MensagemResource', MensagemResource);

  //---

  MensagemResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function MensagemResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/mensagem/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/mensagem/:subpath',
            params : {subpath : 'findwhen'}
        }

    }
    );

    return rest;

  }

});
