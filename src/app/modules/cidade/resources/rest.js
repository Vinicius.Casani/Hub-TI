define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('CidadeResource', CidadeResource);

  CidadeResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function CidadeResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/cidade/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/cidade/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPagination':{
          method: 'GET', isArray:true,
          url:rootUrl + '/estado'
        }

    }
    );

    return rest;

  }

});
