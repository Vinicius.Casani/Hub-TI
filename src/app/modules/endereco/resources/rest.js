define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('EnderecoResource', EnderecoResource);

  //---

  EnderecoResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function EnderecoResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/endereco/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/endereco/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPaginationCidade':{
          method: 'GET', isArray:true,
          url:rootUrl + '/cidade'
        },

        'findPaginationEstado':{
          method: 'GET', isArray:true,
          url:rootUrl + '/estado'
        }

      }
    );

    return rest;

  }

});
