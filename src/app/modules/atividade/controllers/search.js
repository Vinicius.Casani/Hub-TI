define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('AtividadeSearchCtrl', AtividadeSearchCtrl);

  AtividadeSearchCtrl.$inject = [
    'GenericViewService', 'AtividadeResource', '$location', '$filter', 'PaginationFactory' ];

    /**
    * @author Talysson de Castro<talysson.castro@ciss.com.br>
    * @date 14/07/2015
    * @version 1.0
    */
  function AtividadeSearchCtrl( GenericView, Resource, $location, $filter, pagination ) {
    var vm = this;

    pagination = pagination.get('AtividadeSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'atividade', 'idAtividade' );

    //carrega o conteúdo do grid
    vm.initPagination();

    vm.tipoPeriodicidade = tipoPeriodicidadeFn;

    vm.statusTipo = statusTipoFn;

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('atividade/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function statusTipoFn(data){
      return data === 0 ? "Contratual" : "Legal";
    }

    function tipoPeriodicidadeFn(data){
      if (data === 1) {
        return "mensal";
      }
      if(data === 2){
        return "trimestral";
      }
      if(data === 3){
        return "anual";
      }

      return null;
    }

  }

});
