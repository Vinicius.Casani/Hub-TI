define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('AtividadeEmpresaCrudCtrl', AtividadeEmpresaCrudCtrl);

  AtividadeEmpresaCrudCtrl.$inject = ['AtividadeEmpresaResource', '$state', 'GenericCrudService' ];


  function AtividadeEmpresaCrudCtrl(Resource, $state, GenericCrudService ) {

    var vm = this;

    var path = 'atividadeempresa';

    //Título para quando estiver incluindo
    vm.titleIncluir = 'Cadastrar Atividade Empresa';

    //Título para quando estiver editando
    vm.titleEditar = 'Editar Atividade Empresa';

    //Título do formulário
    vm.title = 'Atividade Empresa';

    GenericCrudService.init( vm, vm.titleIncluir, vm.titleEditar, path, Resource, 'idAtividadeEmpresa', onInit );

    function onInit() {

    }

    (function listaTarefas(){
      Resource.findAtividade({id:1},function(result){
        vm.registroAtividade = result;
      });
    })();

  }

});
