define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/atividadeempresa', '/atividadeempresa/list'); // default

    $stateProvider
      .state('atividadeempresa', {
        abstract: true,
        url: '/atividadeempresa',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('atividadeempresa.list', {
        url: '/list',
        views: {
          'content@atividadeempresa': {
            templateUrl   : 'app/modules/atividadeempresa/templates/list.html',
            controller    : 'AtividadeEmpresaSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('atividadeempresa.new', {
        url: '/new',
        views: {
          'content@atividadeempresa': {
            templateUrl   : 'app/modules/atividadeempresa/templates/form.html',
            controller    : 'AtividadeEmpresaCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('atividadeempresa.edit', {
        url: '/edit/:id',
        views: {
          'content@atividadeempresa': {
            templateUrl   : 'app/modules/atividadeempresa/templates/form.html',
            controller    : 'AtividadeEmpresaCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
