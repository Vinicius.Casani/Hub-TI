define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/contato', '/contato/list'); // default

    $stateProvider
      .state('contato', {
        abstract: true,
        url: '/contato',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('contato.list', {
        url: '/list',
        views: {
          'content@contato': {
            templateUrl   : 'app/modules/contato/templates/list.html',
            controller    : 'ContatoSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('contato.new', {
        url: '/new',
        views: {
          'content@contato': {
            templateUrl   : 'app/modules/contato/templates/form.html',
            controller    : 'ContatoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('contato.edit', {
        url: '/edit/:id',
        views: {
          'content@contato': {
            templateUrl   : 'app/modules/contato/templates/form.html',
            controller    : 'ContatoCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
