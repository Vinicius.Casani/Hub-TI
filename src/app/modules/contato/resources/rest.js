define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('ContatoResource', ContatoResource);

  //---

  ContatoResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function ContatoResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/contato/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/contato/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPaginationEmpresa':{
          method: 'GET', isArray:true,
          url:rootUrl + '/empresa'
        },

      }
    );

    return rest;

  }

});
