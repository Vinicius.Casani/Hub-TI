define(function(require) {
  'use strict';

  // TODO: review

  var angular = require('angular');

  // angular module definition
  return angular.module(
    // module name
    'cadastros',

    // module dependencies
      [
      require('./endereco/package').name,
      require('./socio/package').name,
      require('./filial/package').name,
      require('./controleInterno/package').name,
      require('./contato/package').name,
      require('./atividade/package').name,
      require('./empresa/package').name,
      require('./atividadeempresa/package').name,
      require('./cidade/package').name,
      require('./estado/package').name,
      require('./regularidadeFiscal/package').name,
      require('./parecer/package').name,
      require('./notificacao/package').name,
      require('./mensagem/package').name
      ]
  );

});
