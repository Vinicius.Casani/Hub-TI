define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configureStates($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
      .when('/usuario', '/usuario/list'); // default

    $stateProvider
      .state('usuario', {
        abstract: true,
        url: '/usuario',
        views: {
          'master': {
            templateUrl   : 'app/core/main/templates/layout.html'
          }
        }
      })
      .state('usuario.list', {
        url: '/list',
        views: {
          'content@usuario': {
            templateUrl   : 'app/modules/usuario/templates/list.html',
            controller    : 'UsuarioSearchCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('usuario.new', {
        url: '/new',
        views: {
          'content@usuario': {
            templateUrl   : 'app/modules/usuario/templates/form.html',
            controller    : 'UsuarioCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      })
      .state('usuario.edit', {
        url: '/edit/:id',
        views: {
          'content@usuario': {
            templateUrl   : 'app/modules/usuario/templates/form.html',
            controller    : 'UsuarioCrudCtrl',
            controllerAs  : 'vm'
          }
        }
      });

  }

});
