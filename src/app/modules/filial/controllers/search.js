define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('FilialSearchCtrl', FilialSearchCtrl);

  FilialSearchCtrl.$inject = [
    'GenericViewService', 'FilialResource', '$location', '$filter', 'PaginationFactory',
        '$mdDialog'];

    /**
    * @author Luiz Gustavo Schneider<gustavofblack12@gmail.com>
    * @date 02/09/2015
    * @version 1.0
    */
  function FilialSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog ) {
    var vm = this;

    pagination = pagination.get('FilialSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.showAlert=showAlert;

    vm.showConfirm = showConfirmFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'filial', 'idFilial' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('filial/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function showAlert(ev) {
      $mdDialog.show({
        //controller: DialogController,
        templateUrl: 'app/modules/filial/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      });
    }

      function showConfirmFn(ev, filial) {
        var confirm = $mdDialog.confirm()
            .title('Excluir')
            .content('Voce realmente deseja excluir este registro')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('OK')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function() {
          vm.onDeleteClick(filial);
        }, function() {
        });
      }

  }

});
