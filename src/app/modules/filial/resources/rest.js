define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('FilialResource', FilialResource);

  //---

  FilialResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function FilialResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/filial/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/filial/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPagination':{
          method: 'GET', isArray:true,
          url:rootUrl + '/empresa'
        }

    }
    );

    return rest;

  }

});
