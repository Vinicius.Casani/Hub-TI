define(function(require) {
  'use strict';

  var module = require('../module');

  module.controller('RegularidadeFiscalSearchCtrl', RegularidadeFiscalSearchCtrl);

  RegularidadeFiscalSearchCtrl.$inject = [
    'GenericViewService', 'RegularidadeFiscalResource', '$location', '$filter', 'PaginationFactory', '$mdDialog' ];

  function RegularidadeFiscalSearchCtrl( GenericView, Resource, $location, $filter, pagination, $mdDialog) {
    var vm = this;

    pagination = pagination.get('RegularidadeFiscalSearchCtrl');

    //vm.order = orderFn;
    vm.onEditClick = onEditClickFn;

    vm.status = statusFn;

    vm.tipo = tipoFn;

    vm.showConfirm = showConfirmFn;

    // Inicializa o generic: contexto, resource, modulo, id do objeto.
    GenericView.init( vm, Resource, 'regularidadeFiscal', 'idRegularidadeFiscal' );

    //carrega o conteúdo do grid
    vm.initPagination();

    //Ação do editar
    function onEditClickFn(data) {
      $location.path('regularidade_fiscal/edit/' + data.idObjeto);
    }

    // Ordena a lista do grid.
    function orderFn(data) {
      vm.provider = $filter('orderBy')(vm.provider, data.column, !data.desc);
    }

    function statusFn(data){
      return data === 0 ? "True" : "False";
    }

    function tipoFn(data){
      return data === 0 ? "True" : "False";
    }

    function showConfirmFn(ev, regularidadeFiscal) {
      var confirm = $mdDialog.confirm()
          .title('Excluir')
          .content('Voce realmente deseja excluir este registro')
          .ariaLabel('Lucky day')
          .clickOutsideToClose(true)
          .targetEvent(ev)
          .ok('OK')
          .cancel('Cancelar');
      $mdDialog.show(confirm).then(function() {
        vm.onDeleteClick(regularidadeFiscal);
      }, function() {
      });
    }

  }

});
