define(function(require) {
  'use strict';

  var module = require('../module');

  module.factory('RegularidadeFiscalResource', RegularidadeFiscalResource);

  //---

  RegularidadeFiscalResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

  function RegularidadeFiscalResource($resource, rootUrl) {

    var rest = $resource(rootUrl + '/regularidade_fiscal/:id', {}, {
        'update': {
            'method': 'PUT'
        },
        'pagedQuery' : {
            method : 'POST',
            url : rootUrl + '/regularidade_fiscal/:subpath',
            params : {subpath : 'findwhen'}
        },

        'findPagination':{
          method: 'GET', isArray:true,
          url:rootUrl + '/empresa'
        }

    }
    );

    return rest;

  }

});
