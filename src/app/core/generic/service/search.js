define(function(require) {
  'use strict';

  var module = require('./../module');

  module.service('GenericViewService', GenericViewService);

  GenericViewService.$inject = [
    '$stateParams'
  ];

  /**
    * @author Talysson de Castro <talysson.castro@ciss.com.br>
    * @date 22/08/2015
    * @version 1.0
    */
  function GenericViewService(
    $stateParams
  ) {

    return {
      init: initFn
    };

    /**
      * Adiciona o conteúdo da função GenericView ao contexto passado.
      *
      * @param context Contexto do controller.
      * @param Resource Serviço rest para download do conteúdo.
      * @param moduleName Nome do módulo.
      * @param idObject Nome do id dos dados baixados.
      */
    function initFn( context, Resource, moduleName, idObject) {

      GenericView.call(
        context, Resource, moduleName, idObject,
        $stateParams
      );

    }
  }

  /**
    * Função genérica para a tela de search.
    */
  function GenericView(

    Resource, moduleName, idObject, $stateParams

  ) {
    var vm = this;

    var paginador = {
      indicePagina    : 0,    // pagina selecionada
      pageSize        : 50,   // quantidade de itens da página atual
      totalRegistros  : 0,    // total de registros existentes na base de dados
      totalPaginas    : 0,    // total de páginas (totalRegistros/pageSize) encontradas
      ordenacao       : null, // objeto de ordenação: {atributo : "", tipoOrdenacao :[1/0]}
      filtros         : null  // lista de filtros
    };

    var lockLoadData = false;
    var registroExcluido = {};

    vm.colunas = {};
    vm.provider = [];
    vm.formularioPesquisa = {};
    vm.onOrderGrid = onOrderGrid;
    vm.onDeleteClick = onDeleteClick;
    vm.initPagination = initPagination;
    vm.nextPage = nextPage;

    vm.modelResource = modelResourceFn;

    vm.undo = undoFn;

    vm.onSaveSuccess = onSaveSuccessFn;

    var snackBarContext = {
      iconClose: 'odin-icons-close',
      bottom: 'true',
      left: 'true'
    };

    (function verifyInitNew() {

      if( $stateParams.new ) {
        setTimeout(function() {
          //odinDialogService( 'odinSearchBarDialog' ).show( modelResourceFn(), idObject , vm.validate );
        });
      }

    })();

    function modelResourceFn() {
      var model = require('app/modules/' + moduleName + '/model');
      return new Resource( model );
    }

    /**
      * Função executada ao ordenar uma coluna do grid.
      * @param data Coluna a ser ordenada.
      */
    function onOrderGrid( data ) {
      if (data.column) {
        paginador.ordenacao = [{
          atributo: data.column,
          tipoOrdenacao: +data.desc
        }];

        _loadItems();
      }
    }

    function undoFn() {
      var rest = new Resource( registroExcluido );

      rest.$undo(onSuccess, onError);

      function onSuccess(data) {
        console.log('Success', data);

        vm.initPagination();
      }

      function onError( error ) {
        // TODO Analisar como tratar mensagens de erro
        //odinMessageService.error( '', error.data.message, function(){}, error.data );
      }

    }

    /**
      * Ação executada ao deletar uma linha.
      * @param data Objeto a ser deletado.
      */
    function onDeleteClick( data ) {
      Resource.delete({ id: data[idObject] }, onSuccess, onError);

      function onSuccess() {
        vm.initPagination();

        registroExcluido = angular.copy(data);

        showSnackBar();
      }

      function showSnackBar() {
        var context = angular.copy(snackBarContext);
        context.message = 'Registro excluído';
        context.clickButton = vm.undo;
        context.showButton = 'true';
        context.labelButton = 'DESFAZER';

        // odinSnackbar.show( undefined, context );
      }

      function onError( error ) {
        // TODO Analisar como tratar mensagem de erro
        //odinMessageService.error( '', error.data.message, function(){}, error.data );
      }
    }

    function onSaveSuccessFn() {
      vm.initPagination();

      //BreadcrumbService.getAtualName().then(showSnackbar);

      function showSnackbar( name ) {
        snackBarContext.message = name + ' salvo';
        // odinSnackbar.show( undefined, snackBarContext);
      }

    }

    function initPagination(){
      paginador.indicePagina = 0;

      _loadItems();
    }

    /**
      * Carrega os itens da pesquisa com suporte a paginação
      */
    function nextPage( callback ){
      if( paginador.indicePagina >= paginador.totalPaginas ){
        return callback();
      }

      paginador.indicePagina++;

      _loadItems( true, callback );
    }

    /**
      * Método que executa a pesquisa paginada e preenche a lista de items de pesquisa (vm.provider = []).
      */
    function _loadItems( paginado, callback ){
      if ( lockLoadData ) {
        return;
      }

      lockLoadData = true;

      Resource.pagedQuery( paginador, onSuccessPagedQuery );

      /**
        * Função executada quando a requisição retornar com sucesso.
        * @param result Resultado da requisição.
        */
      function onSuccessPagedQuery( result ) {
        result.dataProvider = result.dataProvider || [];

        if( !paginado ){
          vm.provider = [];
        }

        vm.provider = vm.provider.concat( result.dataProvider );

        paginador.indicePagina = result.indicePagina;
        paginador.pageSize = result.pageSize;
        paginador.totalRegistros = result.totalRegistros;
        paginador.totalPaginas = result.totalPaginas;

        lockLoadData = false;

        if( angular.isFunction( callback ) ) {
          callback();
        }

        setOdinSearchBarState( vm.provider.length, paginador.totalRegistros );
      }
    }

    /**
      * Altera o estado do componente odin-search-bar.
      *
      * @param providerSize Total de registros baixados.
      * @param total Total de registros no servidor.
      */
    function setOdinSearchBarState( providerSize, total ) {
      //odinSearchBarService( vm.componentId ).currentRecords( providerSize );
      //odinSearchBarService( vm.componentId ).totalRecords( total );
    }
  }

});
