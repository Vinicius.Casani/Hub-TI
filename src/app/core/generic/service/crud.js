define(function(require) {
  'use strict';

  var module = require('./../module');

  module.service('GenericCrudService', GenericCrudService);

  GenericCrudService.$inject = [];
  //GenericCrudService.$inject.push('BreadcrumbService');
  GenericCrudService.$inject.push('$location');
  GenericCrudService.$inject.push('$timeout');
  GenericCrudService.$inject.push('$stateParams');
  //GenericCrudService.$inject.push('odinMessageService');
  //GenericCrudService.$inject.push('$odinSnackbar');
  GenericCrudService.$inject.push('$rootScope');

  /**
    * @author Talysson de Castro <talysson.castro@ciss.com.br>
    * @date 22/08/2015
    * @version 1.0
    */
  function GenericCrudService( $location, $timeout, $stateParams, $rootScope ) {

    //Valores injetados
    var inject = {
      //BreadcrumbService: BreadcrumbService,
      $location: $location,
      $timeout: $timeout,
      $stateParams: $stateParams,
      $rootScope: $rootScope
    };

    return {
      init: initFn
    };

    /**
      * Adiciona o conteúdo da função GenericView ao contexto passado.
      *
      * @param context Contexto.
      */
    function initFn( context, titleIncluir, titleEditar, path, Resource, idObject, onFinish ) {
      GenericCrud.call( context, titleIncluir, titleEditar, path, Resource, idObject, onFinish, inject );
    }
  }

  function GenericCrud( titleIncluir, titleEditar, path, Resource, idObject, onFinish, inject ) {
    var vm = this;

    //Contexto do snackbar
    var snackBarContext = {
      iconClose: 'odin-icons-close',
      bottom: 'true',
      left: 'true'
    };

    //----------------------------------//

    //Item a ser persistido
    vm.item = {};

    //Mensagens de erro
    vm.errorMessages = {};

    //----------------------------------//

    vm.save = saveFn;

    vm.saveAndIncludeNew = saveAndIncludeNewFn;

    vm.remove = removeFn;

    vm.cancel = onCancelFn;

    vm.goToList = goToList;

    vm.generateNewItem = generateNewItemFn;

    vm.getId = getIdFn;

    vm.disabledSave = false;

    vm.showSnackbar = showSnackbar;

    //----------------------------------//

    //inject.BreadcrumbService.add( vm.getId() ? titleEditar : titleIncluir );

    initData();

    //----------------------------------//

    //Retorna o id dos parametros.
    function getIdFn() {
      return inject.$stateParams.id;
    }

    //Carrega os dados da tela
    function initData() {
      var data = inject.$stateParams.data || {};

      var onFinishCallback = angular.isFunction( onFinish ) ? onFinish : angular.noop;
      var callback = ( data[idObject] ) ? onLoadData : onFinishCallback;

      if( vm.getId() || data[idObject] ) {

        loadData( callback, data[idObject] );

      } else {

        vm.item = vm.generateNewItem();
        callback();

      }

      //Executado ao carregar um item para inclusão
      function onLoadData() {
        delete vm.item[idObject];
        onFinishCallback();
      }

    }

    //Carrega o item para edição
    function loadData( callback, id ) {
      callback = callback || angular.noop;
      id = id || vm.getId();

      Resource.get({ id: id }, onSuccess, onError );

      //Quando der certo, armazena o resultado no item
      function onSuccess( data ) {
        vm.item = new Resource( data );

        callback();
      }

      function onError(error) {
        inject.Message.error( '', error.data.message, angular.noop, error.data );
      }

    }

    //Executado quando o usuário mandar salvar
    function saveFn($event, callback) {
      callback = callback || vm.goToList;
      if(vm.disabledSave) {
        return;
      }

      inject.$timeout(function() {

        vm.disabledSave = true;

        //if( vm.form.$valid ) {

          if( angular.isFunction( vm.beforeSave ) ) {
            vm.beforeSave();
          }

          if( !vm.getId() ) {
            delete vm.item[idObject];
          }

          var isValidForm = angular.isFunction( vm.isValidForm ) ? vm.isValidForm() : true;

          if( isValidForm ) {
            var fn = ( vm.item[idObject] ) ? '$update' : '$save';
            vm.item[fn]( function(){
              vm.disabledSave = false;
              callback();
            }, onSaveError );
          } else {
            vm.disabledSave = false;
          }
        //} else {
          //vm.disabledSave = false;
        //}

        //vm.form.$submitted = false;
      });
    }

    //Executado quando ocorrer um erro ao salvar o item
    function onSaveError( error ) {
      vm.errorMessages = {};
      vm.disabledSave = false;

      showSnackbar( error.data.message );

      angular.forEach(error.data.errors, function (item) {
        vm.errorMessages[item.campo] = item.mensagem;
      });
    }

    //Salva um item e ajusta a tela para uma nova inclusão
    function saveAndIncludeNewFn() {
      vm.save( undefined, onSuccess );

      function onSuccess() {
        vm.item = vm.generateNewItem();

        delete inject.$stateParams.id;
        delete inject.$stateParams.data;

        //inject.BreadcrumbService.removeLast( );
        //inject.BreadcrumbService.add( titleIncluir );

        if( angular.isFunction( onFinish ) ) {
          onFinish();
        }
      }

    }

    //Remove o item
    function removeFn( ) {
      Resource.delete( { id: vm.item[idObject] }, onSuccess, onError );

      function onSuccess() {

        var context = angular.copy(snackBarContext);
        context.clickButton = undo( vm.item );
        context.showButton = 'true';
        context.labelButton = 'CORE_GENERIC_DESFAZER';

        showSnackbar('CORE_GENERIC_REGISTRATION_EXCLUDED', context);

        vm.goToList();
      }

      function onError( error ) {
        // TODO Analisar o código para exibir erros
        //inject.Message.error( '', error.data.message, angular.noop, error.data );
      }
    }

    //Desfaz um excluir
    function undo( item ) {

      return function () {
        var rest = new Resource( item );

        rest.$undo(onSuccess, onError);

        function onSuccess( ) {
          inject.$rootScope.$emit('GENERIC_SEARCH:REFRESH_LIST');
        }

        function onError( error ) {
          // TODO Analisar o código para exibir erros
          //inject.Message.error( '', error.data.message, function(){}, error.data );
        }
      };
    }

    //Executada quando o usuário cancelar
    function onCancelFn() {
      vm.goToList();
    }

    //Direciona para a tela de pesquisa
    function goToList() {
      inject.$location.path( path + '/list' );
    }

    //Exibe o snackbar
    function showSnackbar( mensagem, _context ) {
      var context = _context || angular.copy( snackBarContext );
      context.message = mensagem;

      inject.odinSnackbar.show( undefined, context );
    }

    //Cria um novo resource em branco
    function generateNewItemFn() {
      var model = require('app/modules/' + path + '/model');
      return new Resource( angular.copy( model ) );
    }

  }

});
