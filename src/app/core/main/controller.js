define(function(require) {
  'use strict';

  var module = require('./module');

  module.controller('MainCtrl', MainCtrl);

  //---

  MainCtrl.$inject = ['ProgressConfig', 'MenuConfig', "$mdToast", "NotificacaoResource", "$location", "$mdDialog"];

  function MainCtrl(progressConfig, menu, $mdToast, NotificacaoResource, $location, $mdDialog) {
    var vm = this;

    vm.notificacoes = [];

    vm.detalhesNotificacao = detalhesNotificacaoFn;

    vm.showNotificacao = showNotificacao;

    vm.tipoAtividade = tipoAtividadeFn;

    vm.editarMsgEmail = editarMsgEmailFn;

    var tipo = [];

    vm.sidebarToggle = {
      left: false,
      right: false
    };

    //--- @begin: loading progressbar config
    progressConfig.eventListeners();
    progressConfig.color('#428bca');
    progressConfig.height('3px');
    //--- @end: loading progressbar config

    //--- @begin: menu items
    menu.addMenuItem('Home', 'home');
    menu.addMenuItem('Atividade', 'atividade');
    menu.addMenuItem('Empresa', 'empresa');
    menu.addMenuItem('Atividade Empresa', 'atividadeempresa');
    menu.addMenuItem('Endereco', 'endereco');
    menu.addMenuItem('Sócios', 'socio');
    menu.addMenuItem('Filiais','filial');
    menu.addMenuItem('Controles Internos', 'controleInterno');
    menu.addMenuItem('Contato', 'contato');
    menu.addMenuItem('Regularidade Fiscal','regularidadefiscal');
    menu.addMenuItem('Parecer','parecer');
    menu.addMenuItem('Cidade', 'cidade');
    menu.addMenuItem('Estado', 'estado');
    menu.addMenuItem('Mensagem', 'mensagem');

    //--- @end: menu items


    (function findNotificacoes(){
      NotificacaoResource.findPagination({id:1},function(result){
        vm.notificacoes = result;
      });
    })();

    vm.changeMenu = function() {
      vm.sidebarToggle.left = !vm.sidebarToggle.left;
    };

    vm.closeMenu = function(){
      vm.sidebarToggle.left = false;
    };

    function detalhesNotificacaoFn(registroNotificacao) {
      $location.path('notificacao/detalhes/' + registroNotificacao.idNotificacao);
    }

    function showNotificacao(ev, registroNotificacao) {
      $mdDialog.show({
        controller: PopupController,
        templateUrl: 'app/modules/notificacao/templates/form.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      });

    }

    function PopupController($scope, $mdDialog) {
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.update = function(item) {
        $mdDialog.hide(item);
      };
    }

    function tipoAtividadeFn(data){
      return data === 0 ? "Contratual" : "Legal";
    }

    function editarMsgEmailFn() {
      $location.path('mensagem/edit' + 1);
    }

  }
});
