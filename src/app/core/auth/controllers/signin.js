define(function(require) {
  'use strict';

  var module = require( '../module' );

  module.controller( 'SigninCtrl', SigninCtrl );

  //---

  SigninCtrl.$inject = [ 'AuthService', 'toaster', 'BreadcrumbService' ];

  function SigninCtrl( auth, $toaster, BreadcrumbService ) {

    var vm = this;

    vm.whoami = 'Signin page';

    vm.username = '';
    vm.password = '';

    vm.doLogin = doLogin;

    //---

    setBackground();


    function doLogin() {

      BreadcrumbService.clear();

      var user = {
        username: vm.username,
        password: vm.password
      };

      if(!user.username)
        throw 'username undefined!';

      if(!user.password)
        throw 'password undefined!';

      // TODO: review

      console.log( user );

      auth
        .signin( user )
        .then(function successHandler( result ) {

          console.log( result );

          console.log( 'AuthService' );
          console.log( auth.getToken() );

        }, function errorHandler( result ) {

          $toaster.pop(
            'warning',
            'Login inválido',
            'Usúario ou senha incorretos.'
            );

          console.log( result );

        });

    }

    //---

    function setBackground() {
      var loginContent = document.getElementsByClassName('login-content');

      if( loginContent && loginContent[1] ) {
        var images = 5,
          imgn = Math.floor((Math.random() * images) + 1);

        loginContent[1]
        .style.backgroundImage= 'url(../../../shared/img/background/'+imgn+'.jpg)';
      }
    }

  }

});
