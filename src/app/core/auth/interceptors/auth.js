define(function(require) {
  'use strict';

  var module = require('../module');

  module.config(configure);

  //---

  configure.$inject = [ '$httpProvider', '$provide', '$translateProvider' ];

  function configure( $httpProvider, $provide, $translateProvider ) {

    $provide.factory('AuthRequestConfigIterceptor', AuthRequestConfigIterceptor);
    $provide.factory('AuthResponseInterceptor', AuthResponseInterceptor);

    $httpProvider.interceptors.push('AuthRequestConfigIterceptor');
    $httpProvider.interceptors.push('AuthResponseInterceptor');

    //---

    AuthRequestConfigIterceptor.$inject = [
      'AuthTokenService'
    ];

    function AuthRequestConfigIterceptor( tokenService ) {

      var interceptor = {
        'request': requestHandler
      };

      return interceptor;

      //---

      function requestHandler( config ) {
        config.headers = config.headers || {};
        config.headers['Accept-Language'] = getAcceptLanguage();

        var token = tokenService.getAccessToken();

        if( token ) {
          config.headers.Authorization = 'Bearer ' + token;
        }

        return config;
      }

      // Retorna a linguagem atual usada pelo sistema
      function getAcceptLanguage() {
        var _acceptLanguage = ($translateProvider.use() || 'pt_BR').split('_');

        return [
          _acceptLanguage[0],
          '-',
          _acceptLanguage[1].toUpperCase(),
          ',',
          _acceptLanguage[0],
          ';q=0.8'
        ].join('');
      }

    }

    //---

    AuthResponseInterceptor.$inject = [
      '$q', '$location',
      'AuthPageService', 'AuthTokenService',
      'UserService'
    ];

    function AuthResponseInterceptor( $q, $location, pageService, tokenService, userService ) {

      var interceptor = {
        'responseError': responseErrorHandler
      };

      return interceptor;

      //---

      function responseErrorHandler( response ) {

        // $http: unusable params passed to error handler when CORS preflight fails #3336
        // https://github.com/angular/angular.js/issues/3336
        /*if(
          response.status === 0 ||
          response.status === 401 ||
          response.status === 403
        ) {
          // var token = tokenService.getAccessToken();

          tokenService.set( null );

          // TODO: review
          userService.set( null );

          $location.path( pageService.page.login );
        }*/

        return $q.reject( response );

      }

    }


  }


});
