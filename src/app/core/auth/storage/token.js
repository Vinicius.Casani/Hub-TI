define(function(require) {
  'use strict';

  var _localStorage = window.localStorage;

  var angular = require('angular');

  //---

  var module = require('../module');

  module.factory('AuthTokenStorage', AuthTokenStorage);

  //---

  AuthTokenStorage.$inject = [];

  function AuthTokenStorage() {

    var DEFAULT_EXPIRE_MAX_TIME = 3600; // seconds
    var SUBTRACT_TIME = 10; // seconds

    //----

    var TOKEN_KEY = 'AuthTokenStorage'; // TODO: future change to: ats

    //---

    var service = {
      set: set,
      get: get,
      remove: remove
    };

    return service;

    //---

    function getDS() {
      return _localStorage;
    }

    function set( value ) {
      return getDS()
        .setItem(
          TOKEN_KEY,
          updateTokenToSave( value )
        );
    }

    function get() {
      return updateTokenToReturn(
        getDS()
          .getItem(  TOKEN_KEY  )
      );
    }

    function remove() {
      return getDS()
        .removeItem( TOKEN_KEY );
    }

    //==========================================================================

    function updateTokenToSave( token ) {

      if( angular.isString( token ) ) {
        token = {
          expires_in: DEFAULT_EXPIRE_MAX_TIME,
          access_token: token
        };
      }

      var expires_in = token.expires_in; // seconds
      token.expires_time = (
        Date.now() + (
          ( expires_in - SUBTRACT_TIME ) * 1000
        )
      );

      // TODO: in near future encode BASE64...
      token = JSON.stringify( token );

      return token;

    }

    function updateTokenToReturn( token ) {

      if( !token ) return null;

      // TODO: in near future decode BASE64...
      token = JSON.parse( token );

      if(
        !('expires_time' in token) ||
        ( Date.now() >= token.expires_time )
      ) {
        remove();
        return null;
      }

      return token;
    }

  }

});
