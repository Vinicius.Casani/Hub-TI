describe('Sair:', function() {

  var vm, scope, auth, location;

  // executa antes de cada "it"
  beforeEach(function() {

    // carrega o módulo
    module('core.auth');

    // injeta as dependencias
    inject(function(AuthService, $location, $controller, $rootScope) {

      scope = $rootScope.$new();

      vm = $controller('LogoutCtrl', {
        $scope: scope
      });

      auth = AuthService;
      location = $location;
    });

  });

  it('LogoutCtrl deve ser definido', function() {
    expect(vm).toBeDefined();
  });

  it('AuthService deve ser definido', function() {
    expect(auth).toBeDefined();
  });

  it('não deve estar autenticado', function() {
    expect(auth.isAuthenticated).toEqual(false);
  });

  it('o token deve estar nulo', function() {
    expect(auth.getToken()).toEqual(null);
  });

  it('deve mostrar a página "signin"', function() {
    expect(location.path()).toEqual('/signin');
  });

});
