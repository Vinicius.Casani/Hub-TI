describe("e2e: auth page", function() {
  var page = require('./page.object');

  beforeEach(function() {
    page.get();
  });

  it("should page name : 'Auth Page'", function() {
    // assertions
    expect(page.pageName.getText()).toContain('Auth Page');
  });

});
