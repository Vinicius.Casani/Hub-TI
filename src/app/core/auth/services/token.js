define(function(require) {
  'use strict';

  var angular   = require( 'angular' );
  var module    = require( '../module' );

  var ACCESS_KEY_PARAM = 'accessKey';
  var Base46    = require('shared/libs/Base64');

  module.factory( 'AuthTokenService', AuthTokenService );

  //---

  AuthTokenService.$inject = [ 'AuthTokenStorage', '$window' ];

  function AuthTokenService( storage, $window ) {

    var token = null;

    var service = {
      set: setToken,
      get: getToken,
      getAccessToken: getAccessToken,
      getAccessUrl: generateAccessUrl
    };

    (function() {
      checkAccessKey();
    })();

    // TODO: review and remove
    /*
    $window.tokenService = {
      minAccessObj: minAccessObj,
      expandMinAccessObj: expandMinAccessObj,
      generateAccessKey: generateAccessKey,
      generateAccessUrl: generateAccessUrl
    };
    */

    return service;

    //---

    function setToken( value ) {
      if( value ) {
        storage.set( value );
      } else {
        storage.remove();
      }
    }

    function getToken( key ) {
      var token = storage.get();
      var output = token;
      if( token && angular.isString( key ) ) output = token[ key ];
      return output;
    }

    function getAccessToken( key ) {
      key = key || 'access_token';
      return getToken( key );
    }

    //---

    function checkAccessKey() {
      var accessKey = getURLParameter( ACCESS_KEY_PARAM );
      // TODO: remove
      console.log( accessKey );
      if( accessKey ) {
        var accessStr = Base64.decode( accessKey );
        var accessObj = expandMinAccessObj( accessStr );
        console.log( accessObj ); // TODO: remove
        setToken( accessObj );
      }
    }

    function generateAccessUrl() {
      return $window.location.href + '?' + ACCESS_KEY_PARAM + '=' + encodeURIComponent( generateAccessKey() );
    }

    function generateAccessKey() {
      var str =  minAccessObj();
      return Base64.encode( str );
    }

    //---

    function getURLParameter(name) {
      return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec($window.location.href)||['',''])[1].replace(/\+/g, '%20'))||null;
    }

    //---

    function minAccessObj() {
      var token = getToken();
      if( !token ) return '::';

      var output = '';
      output += token.expires_in + ':';
      output += token.expires_time + ':';
      output += token.access_token;

      return output;
    }

    function expandMinAccessObj( str ) {
      var arr = str.split(':');
      return {
        expires_in   : arr[0],
        expires_time : arr[1],
        access_token : arr[2]
      };
    }

  }

});
