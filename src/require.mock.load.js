define(function(require) {
  'use strict';

  var module = require('shared/mock/package');

  //-------------------
  // @begin: load mocks

  require('app/core/help/mock/allow-pass-github');

  require('app/modules/atividade/mock/package');
  require('app/modules/empresa/mock/package');
  require('app/modules/atividadeempresa/mock/package');
  require('app/modules/endereco/mock/package');
  require('app/modules/socio/mock/package');
  require('app/modules/filial/mock/package');
  require('app/modules/controleInterno/mock/package');
  require('app/modules/contato/mock/package');
  require('app/modules/cidade/mock/package');
  require('appmodules/estado/mock/package');
  require('app/modules/regularidadeFiscal/mock/package');
  require('app/modules/parecer/mock/package');

  // TODO: add here mock module to load


  // @end: load mocks
  //-------------------
  // return mock module
  return module;

});
