define(function(require) {
  'use strict';

  var angular = require('angular');
  var module = angular.module('run');
  //var module = require('./module');

  module.directive('swalWarning', swalWarning);

  //--- https://docs.angularjs.org/guide/directive

  function swalWarning() {

    var directive = {
      restrict: 'A',
      link: linkingFn
    };

    function linkingFn(scope, element, attrs) {

        element.click(function(){
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            });
        });

    }

    return directive;

  }

});
