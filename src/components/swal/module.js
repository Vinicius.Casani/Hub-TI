define(function(require) {
  'use strict';

  var angular = require('angular');
  require('sweetalert');

  // angular module definition
  return angular.module(
    // module name
    'swal',

    // module dependencies
    [
    ]
  );

});
