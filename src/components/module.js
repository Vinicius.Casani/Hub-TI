define(function(require) {
  'use strict';

  // TODO: review

  var angular = require('angular');

  // angular module definition
  return angular.module(
    // module name
    'components',

    // module dependencies
    [
      require('./swal/package').name
    ]
  );

});
